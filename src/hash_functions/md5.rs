//! The MD5 hash algorithm published by R. Rivest in [RFC 1321](https://datatracker.ietf.org/doc/html/rfc1321).
//!
//! WARNING: This algorithm no longer meets the criteria for a cryptographic hash. There
//! are published attacks that efficiently generate collisions for MD5 hashes. One known
//! collision is demonstrated in the test suite for this module.
//!
//! The structure of this code is based on the structure of the reference
//! implementation in RFC 1321. As such, this is a derivative work derived from
//! the RSA Data Security, Inc. MD5 Message-Digest Algorithm.

use super::CryptoHash;

/// The table T, containing the integer part of `4294967296 * abs(sin(i))` for each array index `i`.
// Values copied from the appendix of RFC 1321
const T: [u32; 65] = [
    0x00000000, 0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a, 0xa8304613,
    0xfd469501, 0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 0x6b901122, 0xfd987193, 0xa679438e,
    0x49b40821, 0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d, 0x02441453, 0xd8a1e681,
    0xe7d3fbc8, 0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed, 0xa9e3e905, 0xfcefa3f8, 0x676f02d9,
    0x8d2a4c8a, 0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c, 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60,
    0xbebfbc70, 0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05, 0xd9d4d039, 0xe6db99e5, 0x1fa27cf8,
    0xc4ac5665, 0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92, 0xffeff47d,
    0x85845dd1, 0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 0xf7537e82, 0xbd3af235, 0x2ad7d2bb,
    0xeb86d391,
];

/// Type representing the MD5 hash. It has no data and only exists to implement
/// [`CryptoHash`]
pub struct MD5 {}

impl CryptoHash for MD5 {
    const DIGEST_SIZE: usize = 16;

    fn digest(data: &[u8]) -> [u8; Self::DIGEST_SIZE] {
        let mut blocks = data.array_chunks::<64>();
        let mut state = MD5State::new();

        while let Some(block) = blocks.next() {
            state.update(block);
        }

        state.finalize(blocks.remainder())
    }
}

/// The internal state tracked for MD5 digests. The final values of `a`, `b`,
/// `c`, and `d` are concatenated to produce the message digest.
///
/// The number of bits in the message must be tracked or calculated so it can
/// be appended to the final block.
struct MD5State {
    msg_bits: u64,
    a: u32,
    b: u32,
    c: u32,
    d: u32,
}

impl MD5State {
    /// Initialize the internal state.
    fn new() -> Self {
        Self {
            msg_bits: 0,
            a: u32::from_le_bytes([0x01, 0x23, 0x45, 0x67]),
            b: u32::from_le_bytes([0x89, 0xab, 0xcd, 0xef]),
            c: u32::from_le_bytes([0xfe, 0xdc, 0xba, 0x98]),
            d: u32::from_le_bytes([0x76, 0x54, 0x32, 0x10]),
        }
    }

    fn update(&mut self, block: &[u8; 64]) {
        self.update_inner(&block);

        // increment message length
        self.msg_bits = self.msg_bits.wrapping_add(512);
    }

    /// Update the internal state by digesting a single 512-bit block.
    fn update_inner(&mut self, block: &[u8; 64]) {
        let mut block_as_words = [0; 16];
        for (i, &word_bytes) in block.array_chunks::<4>().enumerate() {
            block_as_words[i] = u32::from_le_bytes(word_bytes);
        }

        let (a, b, c, d) = (self.a, self.b, self.c, self.d);

        // Round 1
        for iteration in 0..4 {
            self.round1_transform(iteration, &block_as_words);
        }

        // Round 2
        for iteration in 0..4 {
            self.round2_transform(iteration, &block_as_words);
        }

        // Round 3
        for iteration in 0..4 {
            self.round3_transform(iteration, &block_as_words);
        }

        // Round 4
        for iteration in 0..4 {
            self.round4_transform(iteration, &block_as_words);
        }

        // Add previous buffer values
        self.a = a.wrapping_add(self.a);
        self.b = b.wrapping_add(self.b);
        self.c = c.wrapping_add(self.c);
        self.d = d.wrapping_add(self.d);
    }

    /// Pad remainder and finalize the digest.
    fn finalize(&mut self, remainder: &[u8]) -> [u8; MD5::DIGEST_SIZE] {
        let remainder_bits = (remainder.len() as u64).wrapping_mul(8);
        let final_blocks = pad_message(remainder, self.msg_bits.wrapping_add(remainder_bits));

        for block in final_blocks {
            self.update_inner(&block);
        }

        // concatenate the bytes of a, b, c, d
        let mut digest = [0; MD5::DIGEST_SIZE];
        digest[..4].copy_from_slice(&self.a.to_le_bytes());
        digest[4..8].copy_from_slice(&self.b.to_le_bytes());
        digest[8..12].copy_from_slice(&self.c.to_le_bytes());
        digest[12..].copy_from_slice(&self.d.to_le_bytes());

        digest
    }

    /// Perform one set of 4 operations defined for Round 1
    ///
    /// note: this depends on observed patterns in the explicitly written-out
    /// operations in RFC 1321
    fn round1_transform(&mut self, iteration: usize, block: &[u32; 16]) {
        let k = 4 * iteration;
        let i = 4 * iteration + 1;

        let aux1 = aux_f(self.b, self.c, self.d);
        self.a = round_op(self.a, self.b, aux1, block[k], 7, i);

        let aux2 = aux_f(self.a, self.b, self.c);
        self.d = round_op(self.d, self.a, aux2, block[k + 1], 12, i + 1);

        let aux3 = aux_f(self.d, self.a, self.b);
        self.c = round_op(self.c, self.d, aux3, block[k + 2], 17, i + 2);

        let aux4 = aux_f(self.c, self.d, self.a);
        self.b = round_op(self.b, self.c, aux4, block[k + 3], 22, i + 3);
    }

    /// Perform one set of 4 operations defined for Round 2
    fn round2_transform(&mut self, iteration: usize, block: &[u32; 16]) {
        let k = (20 * iteration + 1) % 16;
        let i = 4 * iteration + 17;

        let aux1 = aux_g(self.b, self.c, self.d);
        self.a = round_op(self.a, self.b, aux1, block[k], 5, i);

        let aux2 = aux_g(self.a, self.b, self.c);
        let k2 = (k + 5) % 16;
        self.d = round_op(self.d, self.a, aux2, block[k2], 9, i + 1);

        let aux3 = aux_g(self.d, self.a, self.b);
        let k3 = (k + 10) % 16;
        self.c = round_op(self.c, self.d, aux3, block[k3], 14, i + 2);

        let aux4 = aux_g(self.c, self.d, self.a);
        let k4 = (k + 15) % 16;
        self.b = round_op(self.b, self.c, aux4, block[k4], 20, i + 3);
    }

    /// Perform one set of 4 operations defined for Round 3
    fn round3_transform(&mut self, iteration: usize, block: &[u32; 16]) {
        let k = (12 * iteration + 5) % 16;
        let i = 4 * iteration + 33;

        let aux1 = aux_h(self.b, self.c, self.d);
        self.a = round_op(self.a, self.b, aux1, block[k], 4, i);

        let aux2 = aux_h(self.a, self.b, self.c);
        let k2 = (k + 3) % 16;
        self.d = round_op(self.d, self.a, aux2, block[k2], 11, i + 1);

        let aux3 = aux_h(self.d, self.a, self.b);
        let k3 = (k + 6) % 16;
        self.c = round_op(self.c, self.d, aux3, block[k3], 16, i + 2);

        let aux4 = aux_h(self.c, self.d, self.a);
        let k4 = (k + 9) % 16;
        self.b = round_op(self.b, self.c, aux4, block[k4], 23, i + 3);
    }

    /// Perform one set of 4 operations defined for Round 4
    fn round4_transform(&mut self, iteration: usize, block: &[u32; 16]) {
        let k = (28 * iteration) % 16;
        let i = 4 * iteration + 49;

        let aux1 = aux_i(self.b, self.c, self.d);
        self.a = round_op(self.a, self.b, aux1, block[k], 6, i);

        let aux2 = aux_i(self.a, self.b, self.c);
        let k2 = (k + 7) % 16;
        self.d = round_op(self.d, self.a, aux2, block[k2], 10, i + 1);

        let aux3 = aux_i(self.d, self.a, self.b);
        let k3 = (k + 14) % 16;
        self.c = round_op(self.c, self.d, aux3, block[k3], 15, i + 2);

        let aux4 = aux_i(self.c, self.d, self.a);
        let k4 = (k + 21) % 16;
        self.b = round_op(self.b, self.c, aux4, block[k4], 21, i + 3);
    }
}

fn round_op(a: u32, b: u32, aux_result: u32, b_k: u32, s: u32, i: usize) -> u32 {
    b.wrapping_add(
        a.wrapping_add(aux_result.wrapping_add(b_k).wrapping_add(T[i]))
            .rotate_left(s),
    )
}

/// The auxilliary function F defined in RFC 1321 S3.4
fn aux_f(x: u32, y: u32, z: u32) -> u32 {
    (x & y) | (!x & z)
}

/// The auxilliary function G defined in RFC 1321 S3.4
fn aux_g(x: u32, y: u32, z: u32) -> u32 {
    (x & z) | (y & !z)
}

/// The auxilliary function H defined in RFC 1321 S3.4
fn aux_h(x: u32, y: u32, z: u32) -> u32 {
    x ^ y ^ z
}

/// The auxilliary function I defined in RFC 1321 S3.4
fn aux_i(x: u32, y: u32, z: u32) -> u32 {
    y ^ (x | !z)
}

/// Pad the remainder block for MD5. `remainder.len()` should never be greater than 63.
///
/// message_length should be the full length in bits of the message, including the remainder length
fn pad_message(remainder: &[u8], message_length: u64) -> Vec<[u8; 64]> {
    let mut padded = Vec::new();
    let mut block = [0; 64];

    // if remainder's length is 0, this will do nothing
    block[..remainder.len()].copy_from_slice(remainder);

    // set the first bit after the message
    // note: this assumes the message length in bits is congruent to 0 mod 8, i.e.
    // the message consists of an integer number of bytes
    block[remainder.len()] = 0x80;

    let final_block = if remainder.len() < 56 {
        block[56..].copy_from_slice(&message_length.to_le_bytes());
        block
    } else {
        padded.push(block);
        let mut final_block = [0; 64];
        final_block[56..].copy_from_slice(&message_length.to_le_bytes());
        final_block
    };
    padded.push(final_block);

    padded
}

#[cfg(test)]
mod tests {
    use super::*;

    // Empty input test from RFC 1321.
    #[test]
    fn empty_input() {
        let data = [];
        let reference_digest = [
            0xd4, 0x1d, 0x8c, 0xd9, 0x8f, 0x00, 0xb2, 0x04, 0xe9, 0x80, 0x09, 0x98, 0xec, 0xf8,
            0x42, 0x7e,
        ];
        assert_eq!(MD5::digest(&data), reference_digest);
    }

    // All tests from RFC 1321 except the empty input test.
    #[test]
    fn remaining_rfc1321_tests() {
        let data_a = b"a";
        let reference_digest_a = [
            0x0c, 0xc1, 0x75, 0xb9, 0xc0, 0xf1, 0xb6, 0xa8, 0x31, 0xc3, 0x99, 0xe2, 0x69, 0x77,
            0x26, 0x61,
        ];
        let data_abc = b"abc";
        let reference_digest_abc = [
            0x90, 0x01, 0x50, 0x98, 0x3c, 0xd2, 0x4f, 0xb0, 0xd6, 0x96, 0x3f, 0x7d, 0x28, 0xe1,
            0x7f, 0x72,
        ];
        let data1 = b"message digest";
        let reference_digest1 = [
            0xf9, 0x6b, 0x69, 0x7d, 0x7c, 0xb7, 0x93, 0x8d, 0x52, 0x5a, 0x2f, 0x31, 0xaa, 0xf1,
            0x61, 0xd0,
        ];
        let data2 = b"abcdefghijklmnopqrstuvwxyz";
        let reference_digest2 = [
            0xc3, 0xfc, 0xd3, 0xd7, 0x61, 0x92, 0xe4, 0x00, 0x7d, 0xfb, 0x49, 0x6c, 0xca, 0x67,
            0xe1, 0x3b,
        ];
        let data3 = b"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let reference_digest3 = [
            0xd1, 0x74, 0xab, 0x98, 0xd2, 0x77, 0xd9, 0xf5, 0xa5, 0x61, 0x1c, 0x2c, 0x9f, 0x41,
            0x9d, 0x9f,
        ];
        let data4 =
            b"12345678901234567890123456789012345678901234567890123456789012345678901234567890";
        let reference_digest4 = [
            0x57, 0xed, 0xf4, 0xa2, 0x2b, 0xe3, 0xc9, 0x55, 0xac, 0x49, 0xda, 0x2e, 0x21, 0x07,
            0xb6, 0x7a,
        ];

        assert_eq!(MD5::digest(data_a), reference_digest_a);
        assert_eq!(MD5::digest(data_abc), reference_digest_abc);
        assert_eq!(MD5::digest(data1), reference_digest1);
        assert_eq!(MD5::digest(data2), reference_digest2);
        assert_eq!(MD5::digest(data3), reference_digest3);
        assert_eq!(MD5::digest(data4), reference_digest4);
    }

    // test a known collision from https://web.archive.org/web/20140815234704/http://www.rtfm.com/movabletype/archives/2004_08.html#001055
    #[test]
    fn known_collision() {
        let input1 = [
            0xd1, 0x31, 0xdd, 0x02, 0xc5, 0xe6, 0xee, 0xc4, 0x69, 0x3d, 0x9a, 0x06, 0x98, 0xaf,
            0xf9, 0x5c, 0x2f, 0xca, 0xb5, 0x87, 0x12, 0x46, 0x7e, 0xab, 0x40, 0x04, 0x58, 0x3e,
            0xb8, 0xfb, 0x7f, 0x89, 0x55, 0xad, 0x34, 0x06, 0x09, 0xf4, 0xb3, 0x02, 0x83, 0xe4,
            0x88, 0x83, 0x25, 0x71, 0x41, 0x5a, 0x08, 0x51, 0x25, 0xe8, 0xf7, 0xcd, 0xc9, 0x9f,
            0xd9, 0x1d, 0xbd, 0xf2, 0x80, 0x37, 0x3c, 0x5b, 0xd8, 0x82, 0x3e, 0x31, 0x56, 0x34,
            0x8f, 0x5b, 0xae, 0x6d, 0xac, 0xd4, 0x36, 0xc9, 0x19, 0xc6, 0xdd, 0x53, 0xe2, 0xb4,
            0x87, 0xda, 0x03, 0xfd, 0x02, 0x39, 0x63, 0x06, 0xd2, 0x48, 0xcd, 0xa0, 0xe9, 0x9f,
            0x33, 0x42, 0x0f, 0x57, 0x7e, 0xe8, 0xce, 0x54, 0xb6, 0x70, 0x80, 0xa8, 0x0d, 0x1e,
            0xc6, 0x98, 0x21, 0xbc, 0xb6, 0xa8, 0x83, 0x93, 0x96, 0xf9, 0x65, 0x2b, 0x6f, 0xf7,
            0x2a, 0x70,
        ];
        let input2 = [
            0xd1, 0x31, 0xdd, 0x02, 0xc5, 0xe6, 0xee, 0xc4, 0x69, 0x3d, 0x9a, 0x06, 0x98, 0xaf,
            0xf9, 0x5c, 0x2f, 0xca, 0xb5, 0x07, 0x12, 0x46, 0x7e, 0xab, 0x40, 0x04, 0x58, 0x3e,
            0xb8, 0xfb, 0x7f, 0x89, 0x55, 0xad, 0x34, 0x06, 0x09, 0xf4, 0xb3, 0x02, 0x83, 0xe4,
            0x88, 0x83, 0x25, 0xf1, 0x41, 0x5a, 0x08, 0x51, 0x25, 0xe8, 0xf7, 0xcd, 0xc9, 0x9f,
            0xd9, 0x1d, 0xbd, 0x72, 0x80, 0x37, 0x3c, 0x5b, 0xd8, 0x82, 0x3e, 0x31, 0x56, 0x34,
            0x8f, 0x5b, 0xae, 0x6d, 0xac, 0xd4, 0x36, 0xc9, 0x19, 0xc6, 0xdd, 0x53, 0xe2, 0x34,
            0x87, 0xda, 0x03, 0xfd, 0x02, 0x39, 0x63, 0x06, 0xd2, 0x48, 0xcd, 0xa0, 0xe9, 0x9f,
            0x33, 0x42, 0x0f, 0x57, 0x7e, 0xe8, 0xce, 0x54, 0xb6, 0x70, 0x80, 0x28, 0x0d, 0x1e,
            0xc6, 0x98, 0x21, 0xbc, 0xb6, 0xa8, 0x83, 0x93, 0x96, 0xf9, 0x65, 0xab, 0x6f, 0xf7,
            0x2a, 0x70,
        ];

        assert_ne!(input1, input2);
        assert_eq!(MD5::digest(&input1), MD5::digest(&input2));
    }
}
