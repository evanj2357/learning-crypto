//! Implementations of cryptographic hash functions (including some no longer
//! considered suitable for cryptographic use, such as MD5).

pub mod md5;

/// Trait for hash functions.
pub trait CryptoHash {
    /// Size of the hash digest in bytes
    const DIGEST_SIZE: usize;

    /// Compute the hash corresponding to a given input.
    fn digest(data: &[u8]) -> [u8; Self::DIGEST_SIZE];
}
