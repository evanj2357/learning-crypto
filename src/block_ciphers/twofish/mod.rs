//! Implementation of the Twofish cipher as published by Schneier et al.

use super::BlockCipher;

type KeyDependentSBoxes = [[u8; 256]; 4];
const EXPANDED_KEY_WORDS: usize = 40;
const BLOCK_SIZE: usize = 16;
const ROUNDS: usize = 16;

/// Trait representing all implemented variants of the Twofish cipher.
pub trait Twofish {
    /// Key size for each cipher implementation.
    const KEY_SIZE: usize;
}

pub struct Twofish128 {
    expanded_key: [u32; EXPANDED_KEY_WORDS],
    sboxes: KeyDependentSBoxes,
}

pub struct Twofish192 {
    expanded_key: [u32; EXPANDED_KEY_WORDS],
    sboxes: KeyDependentSBoxes,
}

pub struct Twofish256 {
    expanded_key: [u32; EXPANDED_KEY_WORDS],
    sboxes: KeyDependentSBoxes,
}

impl Twofish for Twofish128 {
    const KEY_SIZE: usize = 16;
}
impl Twofish for Twofish192 {
    const KEY_SIZE: usize = 24;
}
impl Twofish for Twofish256 {
    const KEY_SIZE: usize = 32;
}

impl Twofish128 {
    pub fn new(key: [u8; Self::KEY_SIZE]) -> Self {
        let (me, mo, s) = generate_key_schedule::<{ Self::KEY_SIZE }>(key);
        let expanded_key = expand_key::<{ Self::KEY_SIZE }>(me, mo);
        let sboxes = generate_sboxes::<{ Self::KEY_SIZE }>(s);
        Self {
            expanded_key,
            sboxes,
        }
    }
}

impl BlockCipher for Twofish128 {
    const BLOCK_SIZE: usize = BLOCK_SIZE;

    fn encrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        encrypt_block(&self.expanded_key, &self.sboxes, data)
    }

    fn decrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        decrypt_block(&self.expanded_key, &self.sboxes, data)
    }
}

impl Twofish192 {
    pub fn new(key: [u8; Self::KEY_SIZE]) -> Self {
        let (me, mo, s) = generate_key_schedule::<{ Self::KEY_SIZE }>(key);
        let expanded_key = expand_key::<{ Self::KEY_SIZE }>(me, mo);
        let sboxes = generate_sboxes::<{ Self::KEY_SIZE }>(s);
        Self {
            expanded_key,
            sboxes,
        }
    }
}

impl BlockCipher for Twofish192 {
    const BLOCK_SIZE: usize = BLOCK_SIZE;

    fn encrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        encrypt_block(&self.expanded_key, &self.sboxes, data)
    }

    fn decrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        decrypt_block(&self.expanded_key, &self.sboxes, data)
    }
}

impl Twofish256 {
    pub fn new(key: [u8; Self::KEY_SIZE]) -> Self {
        let (me, mo, s) = generate_key_schedule::<{ Self::KEY_SIZE }>(key);
        let expanded_key = expand_key::<{ Self::KEY_SIZE }>(me, mo);
        let sboxes = generate_sboxes::<{ Self::KEY_SIZE }>(s);
        Self {
            expanded_key,
            sboxes,
        }
    }
}

impl BlockCipher for Twofish256 {
    const BLOCK_SIZE: usize = BLOCK_SIZE;

    fn encrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        encrypt_block(&self.expanded_key, &self.sboxes, data)
    }

    fn decrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        decrypt_block(&self.expanded_key, &self.sboxes, data)
    }
}

/// v(x) = x^8 + x^6 + x^5 + x^3 + 1
/// Bitwise, this is (1)01101001. The high coefficient is omitted due to the
/// implementation used for multiplication in GF(2^8).
const VX: u8 = 0x69;

/// w(x) = x^8 + x^6 + x^3 + x^2 + 1
const WX: u8 = 0x4d;

/// The MDS matrix used in the f-function and to generate key-dependent S-boxes.
const MDS: [[u8; 4]; 4] = [
    [0x01, 0xEF, 0x5B, 0x5B],
    [0x5B, 0xEF, 0xEF, 0x01],
    [0xEF, 0x5B, 0x01, 0xEF],
    [0xEF, 0x01, 0xEF, 0x5B],
];

/// The RS matrix used to generate vector S in the key schedule.
const RS: [[u8; 8]; 4] = [
    [0x01, 0xA4, 0x55, 0x87, 0x5A, 0x58, 0xDB, 0x9E],
    [0xA4, 0x56, 0x82, 0xF3, 0x1E, 0xC6, 0x68, 0xE5],
    [0x02, 0xA1, 0xFC, 0xC1, 0x47, 0xAE, 0x3D, 0x19],
    [0xA4, 0x55, 0x87, 0x5A, 0x58, 0xDB, 0x9E, 0x03],
];

const Q0: QPermutation = QPermutation {
    t0: [
        0x8, 0x1, 0x7, 0xd, 0x6, 0xF, 0x3, 0x2, 0x0, 0xB, 0x5, 0x9, 0xE, 0xC, 0xA, 0x4,
    ],
    t1: [
        0xE, 0xC, 0xB, 0x8, 0x1, 0x2, 0x3, 0x5, 0xF, 0x4, 0xA, 0x6, 0x7, 0x0, 0x9, 0xD,
    ],
    t2: [
        0xB, 0xA, 0x5, 0xE, 0x6, 0xD, 0x9, 0x0, 0xC, 0x8, 0xF, 0x3, 0x2, 0x4, 0x7, 0x1,
    ],
    t3: [
        0xD, 0x7, 0xF, 0x4, 0x1, 0x2, 0x6, 0xE, 0x9, 0xB, 0x3, 0x0, 0x8, 0x5, 0xC, 0xA,
    ],
};

const Q1: QPermutation = QPermutation {
    t0: [
        0x2, 0x8, 0xB, 0xD, 0xF, 0x7, 0x6, 0xE, 0x3, 0x1, 0x9, 0x4, 0x0, 0xA, 0xC, 0x5,
    ],
    t1: [
        0x1, 0xE, 0x2, 0xB, 0x4, 0xC, 0x3, 0x7, 0x6, 0xD, 0xA, 0x5, 0xF, 0x9, 0x0, 0x8,
    ],
    t2: [
        0x4, 0xC, 0x7, 0x5, 0x1, 0x6, 0x9, 0xA, 0x0, 0xE, 0xD, 0x8, 0x2, 0xB, 0x3, 0xF,
    ],
    t3: [
        0xB, 0x9, 0x5, 0x1, 0xC, 0x3, 0xD, 0xE, 0x6, 0x4, 0x7, 0xF, 0x2, 0x0, 0x8, 0xA,
    ],
};

struct QPermutation {
    t0: [u8; 16],
    t1: [u8; 16],
    t2: [u8; 16],
    t3: [u8; 16],
}

impl QPermutation {
    fn permute(&self, x: u8) -> u8 {
        let a0 = x.checked_shr(4).unwrap();
        let b0 = x & 0x0f;

        let a1 = a0 ^ b0;
        let b1 = a0 ^ rotate_nibble_right(b0) ^ (8 * a0) % 16;

        let a2 = self.t0[a1 as usize];
        let b2 = self.t1[b1 as usize];

        let a3 = a2 ^ b2;
        let b3 = a2 ^ rotate_nibble_right(b2) ^ (8 * a2) % 16;

        let a4 = self.t2[a3 as usize];
        let b4 = self.t3[b3 as usize];

        16 * b4 + a4
    }
}

fn encrypt_block(
    expanded_key: &[u32; EXPANDED_KEY_WORDS],
    sboxes: &KeyDependentSBoxes,
    block: &[u8; BLOCK_SIZE],
) -> [u8; BLOCK_SIZE] {
    let mut data = [0u32; BLOCK_SIZE / 4];

    for (i, &chunk) in block.array_chunks::<{ BLOCK_SIZE / 4 }>().enumerate() {
        data[i] = u32::from_le_bytes(chunk);
    }

    // input whitening step
    for i in 0..data.len() {
        data[i] ^= expanded_key[i];
    }

    // encryption rounds
    for i in 0..ROUNDS {
        let (f0, f1) = f(
            data[0],
            data[1],
            expanded_key[2 * i + 8],
            expanded_key[2 * i + 9],
            sboxes,
        );
        let r2_next = data[0];
        let r3_next = data[1];
        data[0] = (data[2] ^ f0).rotate_right(1);
        data[1] = data[3].rotate_left(1) ^ f1;
        data[2] = r2_next;
        data[3] = r3_next;
    }

    // undo last swap
    let (temp0, temp1) = (data[0], data[1]);
    data[0] = data[2];
    data[1] = data[3];
    data[2] = temp0;
    data[3] = temp1;

    // output whitening
    for i in 0..data.len() {
        data[i] ^= expanded_key[i + 4];
    }

    let mut ciphertext = [0; BLOCK_SIZE];
    for (i, bytes) in data.map(u32::to_le_bytes).iter().enumerate() {
        ciphertext[4 * i..4 * i + 4].clone_from_slice(bytes);
    }

    ciphertext
}

fn decrypt_block(
    expanded_key: &[u32; EXPANDED_KEY_WORDS],
    sboxes: &KeyDependentSBoxes,
    block: &[u8; BLOCK_SIZE],
) -> [u8; BLOCK_SIZE] {
    let mut data = [0u32; BLOCK_SIZE / 4];

    for (i, &chunk) in block.array_chunks::<{ BLOCK_SIZE / 4 }>().enumerate() {
        data[i] = u32::from_le_bytes(chunk);
    }

    // input whitening step (note that whitening keys must be used in reverse)
    for i in 0..data.len() {
        data[i] ^= expanded_key[i + 4];
    }

    // swap
    let (temp0, temp1) = (data[0], data[1]);
    data[0] = data[2];
    data[1] = data[3];
    data[2] = temp0;
    data[3] = temp1;

    // decryption rounds
    // note: to run the cipher in reverse, the key must be used in reverse
    for i in (0..ROUNDS).rev() {
        let r0_prev = data[2];
        let r1_prev = data[3];
        let (f0, f1) = f(
            r0_prev,
            r1_prev,
            expanded_key[2 * i + 8],
            expanded_key[2 * i + 9],
            sboxes,
        );
        // the rotations must be reversed as well
        data[2] = data[0].rotate_left(1) ^ f0;
        data[3] = (data[1] ^ f1).rotate_right(1);
        data[0] = r0_prev;
        data[1] = r1_prev;
    }

    // output whitening
    for i in 0..data.len() {
        data[i] ^= expanded_key[i];
    }

    let mut plaintext = [0; BLOCK_SIZE];
    for (i, bytes) in data.map(u32::to_le_bytes).iter().enumerate() {
        plaintext[4 * i..4 * i + 4].clone_from_slice(bytes);
    }

    plaintext
}

fn expand_key<const KEY_SIZE: usize>(
    me: [u32; KEY_SIZE / 8],
    mo: [u32; KEY_SIZE / 8],
) -> [u32; EXPANDED_KEY_WORDS] {
    const RHO: u32 = (1 << 24) + (1 << 16) + (1 << 8) + 1;
    let mut expanded_key = [0; EXPANDED_KEY_WORDS];

    for i in 0..(EXPANDED_KEY_WORDS as u32 / 2) {
        let a = h::<KEY_SIZE>(2 * i * RHO, &me);
        let b = h::<KEY_SIZE>((2 * i + 1) * RHO, &mo).rotate_left(8);

        expanded_key[2 * i as usize] = ((a as u64 + b as u64) % (1 << 32)) as u32;
        expanded_key[2 * i as usize + 1] =
            (((a as u64 + 2 * b as u64) % (1 << 32)) as u32).rotate_left(9);
    }

    expanded_key
}

fn generate_key_schedule<const KEY_SIZE: usize>(
    key: [u8; KEY_SIZE],
) -> (
    [u32; KEY_SIZE / 8],
    [u32; KEY_SIZE / 8],
    [u32; KEY_SIZE / 8],
) {
    let mut me = [0; KEY_SIZE / 8];
    let mut mo = [0; KEY_SIZE / 8];
    let mut s = [0; KEY_SIZE / 8];

    let key_words = key
        .array_chunks::<4>()
        .map(|&chunk| u32::from_le_bytes(chunk));
    for (i, kw) in key_words.enumerate() {
        if i % 2 == 0 {
            me[i / 2] = kw;
        } else {
            mo[i / 2] = kw;
        }
    }

    for (i, &chunk) in key.array_chunks::<8>().enumerate() {
        s[s.len() - 1 - i] = u32::from_le_bytes(matrix_multiply_vector(RS, chunk, WX));
    }

    (me, mo, s)
}

fn generate_sboxes<const KEY_SIZE: usize>(l: [u32; KEY_SIZE / 8]) -> KeyDependentSBoxes {
    let mut tables = [[0; 256]; 4];
    for i in 0..=255 {
        let x = u32::from_le_bytes([i; 4]);
        let outputs = h_permutations::<KEY_SIZE>(x, &l);
        for j in 0..4 {
            tables[j][i as usize] = outputs[j];
        }
    }
    tables
}

fn sbox_substitute(x: u32, sboxes: &KeyDependentSBoxes) -> [u8; 4] {
    let bytes = x.to_le_bytes();
    let mut result = [0u8; 4];

    for i in 0..4 {
        result[i] = sboxes[i][bytes[i] as usize];
    }

    result
}

/// The f-function permutes two 32-bit words from the block of data.
///
/// `r0` and `r1` are the input words.
/// `k0` and `k1` are two words of the expanded key.
fn f(r0: u32, r1: u32, k0: u32, k1: u32, sboxes: &KeyDependentSBoxes) -> (u32, u32) {
    let t0 = g(r0, sboxes);
    let t1 = g(r1.rotate_left(8), sboxes);

    let f0 = ((t0 as u64 + t1 as u64 + k0 as u64) % (1 << 32)) as u32;
    let f1 = ((t0 as u64 + 2 * t1 as u64 + k1 as u64) % (1 << 32)) as u32;

    (f0, f1)
}

/// The g-function in Twofish performs a substitution and subsequent matrix
/// multiplication using the field GF(2^8) on the bytes of a 32-bit word.
fn g(x: u32, sboxes: &KeyDependentSBoxes) -> u32 {
    let y = sbox_substitute(x, sboxes);
    let z = matrix_multiply_vector(MDS, y, VX);
    u32::from_le_bytes(z)
}

/// The h-function from Twofish, where KEY_SIZE is specified in bytes.
///
/// Note: This is technically undefined for KEY_SIZE / 8 > 4 and should
/// panic if KEY_SIZE / 8 < 2.
fn h<const KEY_SIZE: usize>(x: u32, l: &[u32; KEY_SIZE / 8]) -> u32 {
    let y = h_permutations::<KEY_SIZE>(x, l);
    u32::from_le_bytes(matrix_multiply_vector(MDS, y, VX))
}

/// The permutations prior to the matrix multiplication in the h-function are
/// implemented separately because they are also needed to pre-compute the
/// key-dependent S-boxes.
fn h_permutations<const KEY_SIZE: usize>(x: u32, l: &[u32; KEY_SIZE / 8]) -> [u8; 4] {
    if KEY_SIZE / 8 < 2 {
        panic!("Key size too small.");
    }

    let y3 = if (KEY_SIZE / 8) >= 4 {
        let y4 = x.to_le_bytes();
        let l3 = l[3].to_le_bytes();
        [
            Q1.permute(y4[0]) ^ l3[0],
            Q0.permute(y4[1]) ^ l3[1],
            Q0.permute(y4[2]) ^ l3[2],
            Q1.permute(y4[3]) ^ l3[3],
        ]
    } else {
        x.to_le_bytes()
    };

    let y2 = if (KEY_SIZE / 8) >= 3 {
        let l2 = l[2].to_le_bytes();
        [
            Q1.permute(y3[0]) ^ l2[0],
            Q1.permute(y3[1]) ^ l2[1],
            Q0.permute(y3[2]) ^ l2[2],
            Q0.permute(y3[3]) ^ l2[3],
        ]
    } else {
        x.to_le_bytes()
    };

    let l1 = l[1].to_le_bytes();
    let l0 = l[0].to_le_bytes();
    [
        Q1.permute(Q0.permute(Q0.permute(y2[0]) ^ l1[0]) ^ l0[0]),
        Q0.permute(Q0.permute(Q1.permute(y2[1]) ^ l1[1]) ^ l0[1]),
        Q1.permute(Q1.permute(Q0.permute(y2[2]) ^ l1[2]) ^ l0[2]),
        Q0.permute(Q1.permute(Q1.permute(y2[3]) ^ l1[3]) ^ l0[3]),
    ]
}

/// Matrix multiplication in GF(2^8)
fn matrix_multiply_vector<const A_ROWS: usize, const B_LEN: usize>(
    a: [[u8; B_LEN]; A_ROWS],
    b: [u8; B_LEN],
    reducing_polynomial: u8,
) -> [u8; A_ROWS] {
    let mut result_vector = [0; A_ROWS];
    for i in 0..A_ROWS {
        result_vector[i] = {
            let mut sum: u8 = 0;
            for j in 0..B_LEN {
                sum ^= multiply_in_gf256(a[i][j], b[j], reducing_polynomial);
            }
            sum
        };
    }
    result_vector
}

/// Multiplication in Galois field GF(2^8) modulo a polynomial represented
/// by a bitstring. Input values are assumed to map to elements of GF(2^8) such that
/// bitwise XOR is equivalent to addition in GF(2^8).
///
/// In this case, the reducing polynomial is assumed to have a term x^8, but
/// only the coefficients up to x^7 are explicit in the function input.
///
/// References:
/// - [helpful answer on StackExchange](https://crypto.stackexchange.com/questions/2700/galois-fields-in-cryptography/2718#2718)
/// - [Wikipedia article](https://en.wikipedia.org/wiki/Finite_field_arithmetic)
fn multiply_in_gf256(a: u8, b: u8, reducing_polynomial: u8) -> u8 {
    let mut accumulator = 0;
    for i in 0..8 {
        accumulator ^= if (1 << i) & b != 0 {
            let mut partial_product = a;
            for _ in 0..i {
                let temp = partial_product << 1;
                if partial_product & 0x80 != 0 {
                    partial_product = temp ^ reducing_polynomial;
                } else {
                    partial_product = temp;
                }
            }
            partial_product
        } else {
            0
        };
    }
    accumulator
}

/// Right rotate the low nibble of a byte. This operation is called
/// ROR4 in the original Twofish description. Since ROR4 in Twofish
/// is used only to rotate nibbles by 1 position, this function is not
/// parameterized on the number of bits to rotate.
fn rotate_nibble_right(x: u8) -> u8 {
    let carry = x & 0x1;
    let msb_mask = 0x07;

    (x.checked_shr(1).unwrap() & msb_mask) | carry.checked_shl(3).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encrypt_block_128_zeroes() {
        let pt = [0; BLOCK_SIZE];
        let key = [0; Twofish128::KEY_SIZE];
        let (me, mo, s) = generate_key_schedule::<{ Twofish128::KEY_SIZE }>(key);
        let expanded_key = expand_key::<{ Twofish128::KEY_SIZE }>(me, mo);
        let sboxes = generate_sboxes::<{ Twofish128::KEY_SIZE }>(s);
        let ct = [
            0x9F, 0x58, 0x9F, 0x5C, 0xF6, 0x12, 0x2C, 0x32, 0xB6, 0xBF, 0xEC, 0x2F, 0x2A, 0xE8,
            0xC3, 0x5A,
        ];

        assert_eq!(encrypt_block(&expanded_key, &sboxes, &pt), ct);
    }

    #[test]
    fn decrypt_block_128_zeroes() {
        let pt = [0; BLOCK_SIZE];
        let key = [0; Twofish128::KEY_SIZE];
        let (me, mo, s) = generate_key_schedule::<{ Twofish128::KEY_SIZE }>(key);
        let expanded_key = expand_key::<{ Twofish128::KEY_SIZE }>(me, mo);
        let sboxes = generate_sboxes::<{ Twofish128::KEY_SIZE }>(s);
        let ct = [
            0x9F, 0x58, 0x9F, 0x5C, 0xF6, 0x12, 0x2C, 0x32, 0xB6, 0xBF, 0xEC, 0x2F, 0x2A, 0xE8,
            0xC3, 0x5A,
        ];

        assert_eq!(decrypt_block(&expanded_key, &sboxes, &ct), pt);
    }

    #[test]
    fn expand_key_zeroes() {
        let key = [0; Twofish128::KEY_SIZE];

        let (me, mo, _) = generate_key_schedule::<{ Twofish128::KEY_SIZE }>(key);
        let expanded_key = expand_key::<{ Twofish128::KEY_SIZE }>(me, mo);

        let correct_expansion = [
            0x52C54DDE, 0x11F0626D, 0x7CAC9D4A, 0x4D1B4AAA, 0xB7B83A10, 0x1E7D0BEB, 0xEE9C341F,
            0xCFE14BE4, 0xF98FFEF9, 0x9C5B3C17, 0x15A48310, 0x342A4D81, 0x424D89FE, 0xC14724A7,
            0x311B834C, 0xFDE87320, 0x3302778F, 0x26CD67B4, 0x7A6C6362, 0xC2BAF60E, 0x3411B994,
            0xD972C87F, 0x84ADB1EA, 0xA7DEE434, 0x54D2960F, 0xA2F7CAA8, 0xA6B8FF8C, 0x8014C425,
            0x6A748D1C, 0xEDBAF720, 0x928EF78C, 0x0338EE13, 0x9949D6BE, 0xC8314176, 0x07C07D68,
            0xECAE7EA7, 0x1FE71844, 0x85C05C89, 0xF298311E, 0x696EA672,
        ];

        assert_eq!(expanded_key, correct_expansion);
    }

    #[test]
    fn h_single_example() {
        let s = [0, 0];
        let x = 0x52C54DDE;

        assert_eq!(h::<{ Twofish128::KEY_SIZE }>(x, &s), 0xC06D4949);
    }

    #[test]
    fn matmul_identity() {
        let matrix = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]];
        let vector = [2, 3, 4, 5];

        assert_eq!(matrix_multiply_vector(matrix, vector, VX), [2, 3, 4, 5]);
    }

    #[test]
    fn matmul() {
        let matrix = [[0, 2, 3, 0], [1, 1, 0, 0], [0, 2, 0, 1], [1, 0, 1, 2]];
        let vector = [1, 2, 3, 4];

        // hand-checked
        //
        // note that this is in GF(2^8) per the Twofish specification,
        // not "normal" arithmetic
        assert_eq!(matrix_multiply_vector(matrix, vector, VX), [1, 3, 0, 10]);
    }

    #[test]
    fn multiplication_in_gf256_identities() {
        assert_eq!(multiply_in_gf256(0x01, 0x32, VX), 0x32);
        assert_eq!(multiply_in_gf256(0x57, 0x01, WX), 0x57);
        assert_eq!(multiply_in_gf256(0x57, 0x01, 0x1b), 0x57);

        assert_eq!(multiply_in_gf256(0x00, 0x00, VX), 0x00);
        assert_eq!(multiply_in_gf256(0x00, 0x41, WX), 0x00);
        assert_eq!(multiply_in_gf256(0x57, 0x00, 0x1b), 0x00);
    }

    #[test]
    fn multiplication_in_gf256() {
        // from FIPS Pub. 197 examples
        assert_eq!(multiply_in_gf256(0x57, 0x08, 0x1b), 0x8e);
    }

    #[test]
    fn ror4() {
        assert_eq!(rotate_nibble_right(0x01), 0x08);
        assert_eq!(rotate_nibble_right(0x0f), 0x0f);
        assert_eq!(rotate_nibble_right(0x06), 0x03);
    }
}
