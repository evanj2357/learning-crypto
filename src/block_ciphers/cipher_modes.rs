/// In Electronic Code Book (ECB) mode, the plaintext is encrypted directly using
/// the same key for every block.
///
/// Caution: ECB mode is vulnerable to statistical analysis regardless of the strength
/// of the cipher used.
pub trait ECBMode: super::BlockCipher {
    /// Encrypt data using a given cipher in Electronic Code Book (ECB) mode. Only complete blocks
    /// will be encrypted, so be sure to properly pad data first.
    fn encrypt_ecb(&self, data: &[u8]) -> Option<Vec<u8>>
    where
        [(); Self::BLOCK_SIZE]: Sized,
    {
        if data.len() % Self::BLOCK_SIZE != 0 {
            None
        } else {
            let data_iter = data.array_chunks::<{ Self::BLOCK_SIZE }>();
            let chunks: Vec<[u8; Self::BLOCK_SIZE]> = data_iter.map(|b| self.encrypt(b)).collect();
            Some(chunks.concat())
        }
    }

    /// Decrypt data using a given cipher in Electronic Code Book (ECB) mode. This will fail if the
    /// input is not evenly divided by the cipher's block size.
    fn decrypt_ecb(&self, data: &[u8]) -> Option<Vec<u8>>
    where
        [(); Self::BLOCK_SIZE]: Sized,
    {
        if data.len() % Self::BLOCK_SIZE != 0 {
            None
        } else {
            let data_iter = data.array_chunks::<{ Self::BLOCK_SIZE }>();
            let chunks: Vec<[u8; Self::BLOCK_SIZE]> = data_iter.map(|b| self.decrypt(b)).collect();
            Some(chunks.concat())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::super::aes::*;
    use super::super::BlockCipher;
    use super::*;

    /*
        data from test vector in FIPS PUB 197 Appendix C.1, repeated twice to ensure
        this tests more than single-block encryption
    */
    const SAMPLE_PLAINTEXT: [u8; 2 * AES128::BLOCK_SIZE] = [
        0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee,
        0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd,
        0xee, 0xff,
    ];
    const SAMPLE_256_BIT_KEY: [u8; AES256::KEY_SIZE] = [
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
        0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d,
        0x1e, 0x1f,
    ];
    const SAMPLE_CIPHERTEXT_256_BIT_KEY: [u8; 2 * AES256::BLOCK_SIZE] = [
        0x8e, 0xa2, 0xb7, 0xca, 0x51, 0x67, 0x45, 0xbf, 0xea, 0xfc, 0x49, 0x90, 0x4b, 0x49, 0x60,
        0x89, 0x8e, 0xa2, 0xb7, 0xca, 0x51, 0x67, 0x45, 0xbf, 0xea, 0xfc, 0x49, 0x90, 0x4b, 0x49,
        0x60, 0x89,
    ];

    #[test]
    fn decrypt_ecb_aes256() {
        let cipher = AES256::new(SAMPLE_256_BIT_KEY);
        assert_eq!(
            &cipher.decrypt_ecb(&SAMPLE_CIPHERTEXT_256_BIT_KEY).unwrap(),
            &SAMPLE_PLAINTEXT
        );
    }

    #[test]
    fn encrypt_ecb_aes256() {
        let cipher = AES256::new(SAMPLE_256_BIT_KEY);
        assert_eq!(
            &cipher.encrypt_ecb(&SAMPLE_PLAINTEXT).unwrap(),
            &SAMPLE_CIPHERTEXT_256_BIT_KEY
        );
    }
}
