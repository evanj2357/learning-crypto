use super::*;

pub type Sbox = [u8; 256];

const RCON: [u32; 11] = [
    0x00000000, 0x01000000, 0x02000000, 0x04000000, 0x08000000, 0x10000000, 0x20000000, 0x40000000,
    0x80000000, 0x1b000000, 0x36000000,
];

/// key expansion per FIPS PUB 197 Section 5.2
///
/// NK: key size in bytes
/// NR: number of rounds
pub fn expand_key<const KEYSIZE: usize, const NR: usize>(
    key: [u8; KEYSIZE],
) -> [u32; BLOCK_WORDS * (NR + 1)]
where
    [u32; BLOCK_WORDS * (NR + 1)]: Sized,
{
    // key size in 32-bit words
    let NK = KEYSIZE as usize / 4;

    let mut expanded_key: [u32; BLOCK_WORDS * (NR + 1)] = [0; BLOCK_WORDS * (NR + 1)];

    for i in 0..NK {
        expanded_key[i] =
            u32::from_be_bytes([key[4 * i], key[4 * i + 1], key[4 * i + 2], key[4 * i + 3]]);
    }

    for i in NK..(BLOCK_WORDS * (NR + 1)) {
        let temp = if i % NK == 0 {
            s_box_word_sub(expanded_key[i - 1].rotate_left(8), encryption::S_BOX) ^ RCON[i / NK]
        } else if NK > 6 && i % NK == 4 {
            s_box_word_sub(expanded_key[i - 1], encryption::S_BOX)
        } else {
            expanded_key[i - 1]
        };

        expanded_key[i] = expanded_key[i - NK] ^ temp;
    }

    expanded_key
}

/// perform S-box substitutions over a block
pub fn s_box_block_sub(block: [u32; BLOCK_WORDS], s_box: Sbox) -> [u32; BLOCK_WORDS] {
    block.map(|word| s_box_word_sub(word, s_box))
}

/// apply an S-box substitution to all bytes of a 32-bit word
fn s_box_word_sub(word: u32, s_box: Sbox) -> u32 {
    u32::from_be_bytes(word.to_be_bytes().map(|x| s_box[x as usize]))
}

/// multiplication modulo m(x) = x^8 + x^4 + x^3 + x + 1 in GF(2^8)
pub fn multiply_mod_mx(a: u8, b: u8) -> u8 {
    (0..8)
        // for each bit index `i`
        .map(|i| {
            if (1 << i) & b != 0 {
                // apply xtime `i` times if bit is set
                (0..i).fold(a, |x, _| xtime(x))
            } else {
                0
            }
        })
        // then xor the resulting bytes
        .fold(0, |acc, n| acc ^ n)
}

/// the xtime operation from FIPS PUB 197 Section 4.2.1
/// (left shift and conditional xor with 0x1b)
fn xtime(b: u8) -> u8 {
    // first shift left 1 position
    let xt = b << 1;
    if b & 0x80 != 0 {
        // then 'subtract' the magic polynomial if input's MSB is set
        xt ^ 0x1b
    } else {
        xt
    }
}

// Tests for AES implementation and helper functions
#[cfg(test)]
mod tests {
    use super::*;

    /*
        example values from FIPS PUB 197, Appendix B.
        Cipher Key = 2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c
    */
    const KEY_U8: [u8; 16] = [
        0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f,
        0x3c,
    ];

    #[test]
    fn xtime() {
        assert_eq!(super::xtime(0x57), 0xae);
        assert_eq!(super::xtime(0xae), 0x47);
        assert_eq!(super::xtime(0x47), 0x8e);
        assert_eq!(super::xtime(0x8e), 0x07);
    }

    #[test]
    fn multiply_mod_mx() {
        assert_eq!(super::multiply_mod_mx(0x57, 0x01), 0x57);
        assert_eq!(super::multiply_mod_mx(0x57, 0x02), 0xae);
        assert_eq!(super::multiply_mod_mx(0x57, 0x04), 0x47);
        assert_eq!(super::multiply_mod_mx(0x57, 0x08), 0x8e);
        assert_eq!(super::multiply_mod_mx(0x57, 0x10), 0x07);
        assert_eq!(super::multiply_mod_mx(0x57, 0x13), 0xfe);
    }

    #[test]
    fn expand_key_128() {
        assert_eq!(
            super::expand_key::<{ AES128::KEY_SIZE }, { AES128::ROUNDS }>(KEY_U8),
            [
                0x2b7e1516, 0x28aed2a6, 0xabf71588, 0x09cf4f3c, 0xa0fafe17, 0x88542cb1, 0x23a33939,
                0x2a6c7605, 0xf2c295f2, 0x7a96b943, 0x5935807a, 0x7359f67f, 0x3d80477d, 0x4716fe3e,
                0x1e237e44, 0x6d7a883b, 0xef44a541, 0xa8525b7f, 0xb671253b, 0xdb0bad00, 0xd4d1c6f8,
                0x7c839d87, 0xcaf2b8bc, 0x11f915bc, 0x6d88a37a, 0x110b3efd, 0xdbf98641, 0xca0093fd,
                0x4e54f70e, 0x5f5fc9f3, 0x84a64fb2, 0x4ea6dc4f, 0xead27321, 0xb58dbad2, 0x312bf560,
                0x7f8d292f, 0xac7766f3, 0x19fadc21, 0x28d12941, 0x575c006e, 0xd014f9a8, 0xc9ee2589,
                0xe13f0cc8, 0xb6630ca6
            ]
        );
    }

    #[test]
    fn expand_key_256() {
        // data from test vector in FIPS PUB 197 Appendix A.3
        let key = [
            0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d,
            0x77, 0x81, 0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3,
            0x09, 0x14, 0xdf, 0xf4,
        ];
        let correct_expansion = [
            0x603deb10, 0x15ca71be, 0x2b73aef0, 0x857d7781, 0x1f352c07, 0x3b6108d7, 0x2d9810a3,
            0x0914dff4, 0x9ba35411, 0x8e6925af, 0xa51a8b5f, 0x2067fcde, 0xa8b09c1a, 0x93d194cd,
            0xbe49846e, 0xb75d5b9a, 0xd59aecb8, 0x5bf3c917, 0xfee94248, 0xde8ebe96, 0xb5a9328a,
            0x2678a647, 0x98312229, 0x2f6c79b3, 0x812c81ad, 0xdadf48ba, 0x24360af2, 0xfab8b464,
            0x98c5bfc9, 0xbebd198e, 0x268c3ba7, 0x09e04214, 0x68007bac, 0xb2df3316, 0x96e939e4,
            0x6c518d80, 0xc814e204, 0x76a9fb8a, 0x5025c02d, 0x59c58239, 0xde136967, 0x6ccc5a71,
            0xfa256395, 0x9674ee15, 0x5886ca5d, 0x2e2f31d7, 0x7e0af1fa, 0x27cf73c3, 0x749c47ab,
            0x18501dda, 0xe2757e4f, 0x7401905a, 0xcafaaae3, 0xe4d59b34, 0x9adf6ace, 0xbd10190d,
            0xfe4890d1, 0xe6188d0b, 0x046df344, 0x706c631e,
        ];

        assert_eq!(
            expand_key::<{ AES256::KEY_SIZE }, { AES256::ROUNDS }>(key),
            correct_expansion
        );
    }

    #[test]
    fn apply_s_box() {
        // data from test vector in FIPS PUB 197 Appendix C.1
        let start_data = [0x00102030, 0x40506070, 0x8090a0b0, 0xc0d0e0f0];
        let end_data = [0x63cab704, 0x0953d051, 0xcd60e0e7, 0xba70e18c];

        assert_eq!(s_box_block_sub(start_data, encryption::S_BOX), end_data);
    }

    #[test]
    fn inv_apply_s_box() {
        // data from test vector in FIPS PUB 197 Appendix C.1, inverse cipher round 2
        let inv_start_data = [0x5411f4b5, 0x6bd9700e, 0x96a0902f, 0xa1bb9aa1];
        let inv_end_data = [0xfde3bad2, 0x05e5d0d7, 0x3547964e, 0xf1fe37f1];

        assert_eq!(
            s_box_block_sub(inv_start_data, decryption::INV_S_BOX),
            inv_end_data
        );
    }
}
