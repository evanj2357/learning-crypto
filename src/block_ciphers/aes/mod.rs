//! AES block cipher implementation following FIPS Pub. 197
//!
//! Functions in this module encrypt and decrypt single 128-bit (16-byte) blocks using
//! 128-, 192-, or 256-bit keys. Note that they take fixed-sized arrays rather than
//! slices--this is intentional because the algorithm specifies a fixed block size.
//! The caller should pad plaintexts as needed before encrypting them.
//!
//! ```
//! use learning_crypto::block_ciphers::BlockCipher;
//! use learning_crypto::block_ciphers::aes::*;
//!
//! let plaintext_block: [u8; AES192::BLOCK_SIZE] = [
//!     0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd,
//!     0xee, 0xff,
//! ];
//! let key: [u8; AES192::KEY_SIZE] = [
//!     0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
//!     0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
//! ];
//!
//! let cipher = AES192::new(key);
//!
//! let ciphertext_block = cipher.encrypt(&plaintext_block);
//!
//! let decrypted_block = cipher.decrypt(&ciphertext_block);
//!
//! assert_eq!(decrypted_block, plaintext_block);
//! ```

mod decryption;
mod encryption;
mod utilities;

use super::BlockCipher;
use utilities::*;

/// The block size in bytes - AES always encrypts/decrypts exactly 128 bytes at a time
/// regardless of key size.
pub const AES_BLOCK_SIZE: usize = 16;
const BLOCKSIZE: usize = AES_BLOCK_SIZE;

/// Block size as number of 32-bit words ('Nb' in FIPS PUB 197)
const BLOCK_WORDS: usize = BLOCKSIZE / 4;

const fn key_expansion_size(rounds: usize) -> usize {
    BLOCK_WORDS * (rounds + 1)
}

pub struct AES128 {
    expanded_key: [u32; key_expansion_size(Self::ROUNDS)],
}

pub struct AES192 {
    expanded_key: [u32; key_expansion_size(Self::ROUNDS)],
}

pub struct AES256 {
    expanded_key: [u32; key_expansion_size(Self::ROUNDS)],
}

pub trait AES {
    const KEY_SIZE: usize;
    const ROUNDS: usize;
}

impl AES for AES128 {
    const KEY_SIZE: usize = 16;
    const ROUNDS: usize = 10;
}

impl AES for AES192 {
    const KEY_SIZE: usize = 24;
    const ROUNDS: usize = 12;
}

impl AES for AES256 {
    const KEY_SIZE: usize = 32;
    const ROUNDS: usize = 14;
}

impl AES128 {
    pub fn new(key: [u8; Self::KEY_SIZE]) -> Self {
        Self {
            expanded_key: expand_key::<{ Self::KEY_SIZE }, { Self::ROUNDS }>(key),
        }
    }
}

impl BlockCipher for AES128 {
    const BLOCK_SIZE: usize = BLOCKSIZE;

    fn encrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        encryption::encrypt_block::<{ Self::ROUNDS }, { key_expansion_size(Self::ROUNDS) }>(
            data,
            &self.expanded_key,
        )
    }

    fn decrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        decryption::decrypt_block::<{ Self::ROUNDS }, { key_expansion_size(Self::ROUNDS) }>(
            data,
            &self.expanded_key,
        )
    }
}

impl AES192 {
    pub fn new(key: [u8; Self::KEY_SIZE]) -> Self {
        Self {
            expanded_key: expand_key::<{ Self::KEY_SIZE }, { Self::ROUNDS }>(key),
        }
    }
}

impl BlockCipher for AES192 {
    const BLOCK_SIZE: usize = BLOCKSIZE;

    fn encrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        encryption::encrypt_block::<{ Self::ROUNDS }, { key_expansion_size(Self::ROUNDS) }>(
            data,
            &self.expanded_key,
        )
    }

    fn decrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        decryption::decrypt_block::<{ Self::ROUNDS }, { key_expansion_size(Self::ROUNDS) }>(
            data,
            &self.expanded_key,
        )
    }
}

impl AES256 {
    pub fn new(key: [u8; Self::KEY_SIZE]) -> Self {
        Self {
            expanded_key: expand_key::<{ Self::KEY_SIZE }, { Self::ROUNDS }>(key),
        }
    }
}

impl BlockCipher for AES256 {
    const BLOCK_SIZE: usize = BLOCKSIZE;

    fn encrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        encryption::encrypt_block::<{ Self::ROUNDS }, { key_expansion_size(Self::ROUNDS) }>(
            data,
            &self.expanded_key,
        )
    }

    fn decrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE] {
        decryption::decrypt_block::<{ Self::ROUNDS }, { key_expansion_size(Self::ROUNDS) }>(
            data,
            &self.expanded_key,
        )
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Block {
    data: [u32; BLOCK_WORDS],
}

impl Block {
    pub fn new(block_data: &[u8; BLOCKSIZE]) -> Self {
        let mut data: [u32; BLOCK_WORDS] = [0; BLOCK_WORDS];

        for i in 0..BLOCK_WORDS {
            data[i] = u32::from_be_bytes([
                block_data[4 * i],
                block_data[4 * i + 1],
                block_data[4 * i + 2],
                block_data[4 * i + 3],
            ]);
        }

        Block { data }
    }

    /// get raw bytes of the block
    pub fn to_bytes(self) -> [u8; BLOCKSIZE] {
        let mut data: [u8; BLOCKSIZE] = [0; BLOCKSIZE];

        for i in 0..BLOCK_WORDS {
            let c = u32::to_be_bytes(self.data[i]);
            for j in 0..4 {
                data[4 * i + j] = c[j];
            }
        }

        data
    }

    pub fn add_round_key(self, wr: [u32; BLOCK_WORDS]) -> Self {
        let data = self.data.zip(wr).map(|(x, w)| x ^ w);
        Self { data }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /*
        example values from FIPS PUB 197, Appendix B.
        Cipher Key = 2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c
    */
    const KEY_U32: [u32; 4] = [0x2b7e1516, 0x28aed2a6, 0xabf71588, 0x09cf4f3c];

    #[test]
    fn add_round_key() {
        /*
            example values from FIPS PUB 197, Appendix B.
            Input = 32 43 f6 a8 88 5a 30 8d 31 31 98 a2 e0 37 07 34
            Cipher Key = 2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c
        */
        let initial_state = Block {
            data: [0x3243f6a8, 0x885a308d, 0x313198a2, 0xe0370734],
        };
        let next_state = Block {
            data: [0x193de3be, 0xa0f4e22b, 0x9ac68d2a, 0xe9f84808],
        };
        assert_eq!(initial_state.add_round_key(KEY_U32), next_state);
    }
}
