use super::*;

/// Substitution bytes box
pub const S_BOX: [u8; 256] = [
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
];

/// Mix column Coefficients
const MIXCOL_COEFFS: [[u8; 4]; 4] = [
    [02, 03, 01, 01],
    [01, 02, 03, 01],
    [01, 01, 02, 03],
    [03, 01, 01, 02],
];

/// block encryption per FIPS PUB 197 Section 5.2
pub fn encrypt_block<const ROUNDS: usize, const KEY_EXPANSION_SIZE: usize>(
    block: &[u8; BLOCKSIZE],
    key: &[u32; KEY_EXPANSION_SIZE],
) -> [u8; BLOCKSIZE]
where
    [u32; KEY_EXPANSION_SIZE]: Sized,
{
    let mut expanded_key = key.array_chunks::<{ BLOCK_WORDS }>();

    let initial_state = Block::new(block);

    let key_added = initial_state.add_round_key(*expanded_key.next().unwrap());

    let state = (1..ROUNDS).fold(key_added, |state, _round| {
        state
            .sub_bytes()
            .shift_rows()
            .mix_columns()
            .add_round_key(*expanded_key.next().unwrap())
    });

    state
        .sub_bytes()
        .shift_rows()
        .add_round_key(*expanded_key.next().unwrap())
        .to_bytes()
}

// Encryption state transitions

impl Block {
    pub fn sub_bytes(self) -> Self {
        Self {
            data: s_box_block_sub(self.data, S_BOX),
        }
    }

    pub fn shift_rows(self) -> Self {
        let block_bytes = self.data.map(|w| w.to_be_bytes());

        let new_block_bytes = [0, 1, 2, 3].map(|r| {
            let row = u32::from_be_bytes([
                block_bytes[0][r],
                block_bytes[1][r],
                block_bytes[2][r],
                block_bytes[3][r],
            ]);
            row.rotate_left(8 * r as u32).to_be_bytes()
        });

        let data = [0, 1, 2, 3].map(|c| u32::from_be_bytes(new_block_bytes.map(|r| r[c])));

        Self { data }
    }

    pub fn mix_columns(self) -> Self {
        let data = self.data.map(|w| {
            let w_bytes = w.to_be_bytes();
            u32::from_be_bytes([0, 1, 2, 3].map(|i| {
                multiply_mod_mx(MIXCOL_COEFFS[i][0], w_bytes[0])
                    ^ multiply_mod_mx(MIXCOL_COEFFS[i][1], w_bytes[1])
                    ^ multiply_mod_mx(MIXCOL_COEFFS[i][2], w_bytes[2])
                    ^ multiply_mod_mx(MIXCOL_COEFFS[i][3], w_bytes[3])
            }))
        });

        Self { data }
    }
}

// Tests for AES implementation and helper functions
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn encrypt_block_128() {
        // data from test vector in FIPS PUB 197 Appendix C.1
        let pt = [
            0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd,
            0xee, 0xff,
        ];
        let key = [
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
            0x0e, 0x0f,
        ];
        let correct_ct = [
            0x69, 0xc4, 0xe0, 0xd8, 0x6a, 0x7b, 0x04, 0x30, 0xd8, 0xcd, 0xb7, 0x80, 0x70, 0xb4,
            0xc5, 0x5a,
        ];

        assert_eq!(
            encrypt_block::<{ AES128::ROUNDS }, { key_expansion_size(AES128::ROUNDS) }>(
                &pt,
                &expand_key::<{ AES128::KEY_SIZE }, { AES128::ROUNDS }>(key)
            ),
            correct_ct
        );
    }

    #[test]
    fn encrypt_block_256() {
        // data from test vector in FIPS PUB 197 Appendix C.3
        let pt = [
            0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd,
            0xee, 0xff,
        ];
        let key = [
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d,
            0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b,
            0x1c, 0x1d, 0x1e, 0x1f,
        ];
        let correct_ct = [
            0x8e, 0xa2, 0xb7, 0xca, 0x51, 0x67, 0x45, 0xbf, 0xea, 0xfc, 0x49, 0x90, 0x4b, 0x49,
            0x60, 0x89,
        ];

        assert_eq!(
            encrypt_block::<{ AES256::ROUNDS }, { key_expansion_size(AES256::ROUNDS) }>(
                &pt,
                &expand_key::<{ AES256::KEY_SIZE }, { AES256::ROUNDS }>(key)
            ),
            correct_ct
        );
    }

    #[test]
    fn sub_bytes() {
        // data from test vector in FIPS PUB 197 Appendix C.1
        let start_data = [0x00102030, 0x40506070, 0x8090a0b0, 0xc0d0e0f0];
        let end_data = [0x63cab704, 0x0953d051, 0xcd60e0e7, 0xba70e18c];

        assert_eq!(
            Block { data: start_data }.sub_bytes(),
            Block { data: end_data }
        );
    }

    #[test]
    fn shift_rows() {
        // data from test vector in FIPS PUB 197 Appendix C.1, forward cipher round 1
        let start_data = [0x63cab704, 0x0953d051, 0xcd60e0e7, 0xba70e18c];
        let end_data = [0x6353e08c, 0x0960e104, 0xcd70b751, 0xbacad0e7];

        assert_eq!(
            Block { data: start_data }.shift_rows(),
            Block { data: end_data }
        );
    }

    #[test]
    fn mix_columns() {
        // data from test vector in FIPS PUB 197 Appendix C.1
        let start_data = [0x6353e08c, 0x0960e104, 0xcd70b751, 0xbacad0e7];
        let end_data = [0x5f726415, 0x57f5bc92, 0xf7be3b29, 0x1db9f91a];

        assert_eq!(
            Block { data: start_data }.mix_columns(),
            Block { data: end_data }
        );
    }
}
