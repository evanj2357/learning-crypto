//! Block cipher implementations.
//!
//! Cipher modes are implemented for all ciphers, so usage follows a consistent
//! pattern:
//!
//! ```
//! use learning_crypto::block_ciphers::aes::*;
//! use learning_crypto::block_ciphers::{BlockCipher, ECBMode};
//!
//! const SAMPLE_AES128_KEY: [u8; AES128::KEY_SIZE] = [
//!     0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
//!     0x0f,
//! ];
//! const SAMPLE_PLAINTEXT: [u8; 2 * AES128::BLOCK_SIZE] = [
//!     0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee,
//!     0xff, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd,
//!     0xee, 0xff,
//! ];
//! let sample_ciphertext: Vec<u8> = vec![
//!     0x69, 0xc4, 0xe0, 0xd8, 0x6a, 0x7b, 0x04, 0x30, 0xd8, 0xcd, 0xb7, 0x80, 0x70, 0xb4, 0xc5,
//!     0x5a, 0x69, 0xc4, 0xe0, 0xd8, 0x6a, 0x7b, 0x04, 0x30, 0xd8, 0xcd, 0xb7, 0x80, 0x70, 0xb4,
//!     0xc5, 0x5a,
//! ];
//!
//! let cipher = AES128::new(SAMPLE_AES128_KEY);
//!
//! let ciphertext = cipher.encrypt_ecb(&SAMPLE_PLAINTEXT).unwrap();
//!
//! assert_eq!(&sample_ciphertext, &ciphertext);
//! ```
//! _(Example data from FIPS Pub. 197 Appendix C.1)_

pub mod aes;
mod cipher_modes;
pub mod twofish;

pub use cipher_modes::ECBMode;

/// The ciphers that are supported
#[derive(Debug, PartialEq, Eq)]
pub enum Cipher {
    /// AES with 128-bit keys
    AES128,
    /// AES with 196-bit keys
    AES192,
    /// AES with 256-bit keys
    AES256,
}

/// The supported modes for encryption
#[derive(Debug, PartialEq, Eq)]
pub enum Mode {
    /// Electronic Code Book mode
    ECB,
}

/// Trait representing a generic block cipher.
pub trait BlockCipher {
    /// Block size defined for the cipher.
    const BLOCK_SIZE: usize;

    /// Encrypt a single block of data.
    fn encrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE];

    /// Decrypt a single block of data.
    fn decrypt(&self, data: &[u8; Self::BLOCK_SIZE]) -> [u8; Self::BLOCK_SIZE];
}

// usees the default implementation
impl<Cipher> ECBMode for Cipher where Cipher: BlockCipher {}
