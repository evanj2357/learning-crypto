//! Classical ciphers!
//!
//! These are _not_ secure in any sense, but they're fun to play with
//! and a great introduction to cryptography concepts!

/// Caesar cipher implementation on ASCII alphabetic characters.
///
/// Non-ASCII and non-alphabetic characters are passed through unchanged.
pub fn caesar_cipher(input: &str, shift: i32) -> String {
    input
        .chars()
        .map(|c| single_char_caesar(c, shift))
        .collect()
}

/// Single-character alphabetic rotation. A helpful building block
/// for the Caesar cipher and Viginere cipher.
///
/// Non-ASCII and non-alphabetic characters are passed through unchanged.
fn single_char_caesar(c: char, shift: i32) -> char {
    if c.is_ascii_lowercase() {
        let rel_0 = (c as u32) - ('a' as u32);

        // Rust's % operator produces values in (-m, +m)
        let offset = if shift >= 0 {
            shift % 26
        } else {
            26 - (shift % 26).abs()
        };
        char::from_u32(((rel_0 + offset as u32) % 26) + ('a' as u32)).unwrap()
    } else if c.is_ascii_uppercase() {
        let rel_0 = (c as u32) - ('A' as u32);

        // Rust's % operator produces values in (-m, +m)
        let offset = if shift >= 0 {
            shift % 26
        } else {
            26 - (shift % 26).abs()
        };
        char::from_u32(((rel_0 + offset as u32) % 26) + ('A' as u32)).unwrap()
    } else {
        c
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn single_char_caesar() {
        assert_eq!(super::single_char_caesar('a', 1), 'b');
        assert_eq!(super::single_char_caesar('a', 27), 'b');
        assert_eq!(super::single_char_caesar('a', -25), 'b');
    }

    #[test]
    fn caesar_cipher() {
        assert_eq!(super::caesar_cipher("abcd", 1), "bcde");
    }
}
