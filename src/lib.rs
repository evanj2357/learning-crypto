#![warn(missing_docs)]
#![feature(array_chunks)]
#![feature(array_map)]
#![feature(array_zip)]
#![feature(const_generics)]
#![feature(const_evaluatable_checked)]
#![feature(assoc_char_funcs)]

/// Implementations of block ciphers such as AES (Rjindael)
pub mod block_ciphers;

/// Implementations of classical ciphers such as Caesar, Vigenere, and others
/// based on substitutions and permutations.
pub mod classical_ciphers;

pub mod hash_functions;
