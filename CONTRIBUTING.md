# Guidelines for Contributing

## Documentation and language

- Use inclusive language. Avoid racist, classist, ableist, or
otherwise offensive terms.

- All documentation should be in English.

## Behavior/Conduct

1. Be kind.

2. For specific details, the [Mozilla Community Participation Guidelines](https://www.mozilla.org/en-US/about/governance/policies/participation/)
are a good place to start.

3. See (1).

## Coding Style

The primary style guideline is "keep it reasonably readable." To that end,
some specific points are:

- Run `cargo fmt` before committing code.

- Use descriptive variable names. For the most part, that means using words,
but sometimes, particularly when implementing ciphers, the most descriptive
name is the 1-3 letter name given to a variable in the canonical algorithm
description. Single-letter loop indices are fine, too.

- Try not to exceed 5 levels of indentation.

## Ideas/To-do

If it's cryptography-relevant, it probably has a place here. AES? Yes!
Caesar cipher? Also yes! The whole point of this repository/project
is learning. If there's a Rust feature you want to practice with, find
an excuse. If there's a cipher or protocol you want to learn more about,
try writing your own implementation or check out an existing one!
Multiple implementations of the same algorithm are welcome.

Don't be afraid to just add something. Do give it a nice home in the project
structure.

If you're looking for inspiration, a to-do list/ideas list will appear at
some point.
