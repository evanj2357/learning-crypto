# Learning Cryptography (in Rust)!

_DISCLAIMER: THIS IS PRACTICE CODE FOR LEARNING PURPOSES._
_**DO NOT** USE THIS CODE IN SENSITIVE APPLICATIONS. THIS CODE IS_
_PRESENTED WITHOUT WARRANTY, ETC., ETC._

## Contributors 

### Evan Johnson (he/him)

Evan is a graduate student at Portland State University. He also has
a BA in rocks and enjoys puns just a little too much.

## About that disclaimer

Most or all of the code in this repository has been written and tested
by exactly one person. That alone should be enough to scare you out of
using it in real-world applications. Additionally, none of this code
has gone through any sort of trusted or official validation process.

That said, you're more than welcome to use this code for your own
learning purposes! Play around with it, test it, critique it, etc. to
your heart's content. This project began with a desire to learn about
cryptography, and it would make no sense to lock it away or prevent
others from using it to learn :-)

All I ask is that if you decide you want to encrypt actual sensitive
data, _use a reputable, well-tested, validated implementation of your_
_chosen cipher or hash function_.

## What's all this about?

I'm trying to learn about cryptography and Rust by implementing ciphers,
cryptanalysis, encryption protocols, and some well-known published attacks
against ciphers and protocols. I'd like to learn with friends, so I'll
be inviting some friends to contribute to this project, too.

## What's in this repository?

Right now, there's just an AES implementation. In the future, there
will also be other ciphers, example programs, some fun stuff with
classical ciphers, better documentation, etc.

### Directory structure

The `src` directory contains a Rust library crate that exports a
various submodules.

Example programs using the library code live in the `examples`
directory (or they will, once some exist).

## Usage

This project requires Rust nightly to build, at least until a few features
are stabilized.

### As a library dependency

Add `learning-crypto = "0.1.0"` to the dependencies section of your
`Cargo.toml` file.

### Running examples
Once example programs exist, the `cargo run --example`
command can be used to run them.
